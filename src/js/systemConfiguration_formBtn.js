$(window).ready(function(){

    /*
        Basic Info --- Edit Button
    */
    $(".ui_editBtnBasicInfo").on("click", function(){
        $(".ui_basicInfoEmailText").addClass("hidden")
        $(".ui_basicInfoUsernameText").addClass("hidden")
        $(".ui_basicInfoFullNameText").addClass("hidden")
        $(".ui_basicInfoRoleText").addClass("hidden")
        $(this).addClass("hidden")

        $(".ui_basicInfoEmailInput").removeClass("hidden")
        $(".ui_basicInfoUsernameInput").removeClass("hidden")
        $(".ui_basicInfoFullNameInput").removeClass("hidden")
        $(".ui_basicInfoRoleInput").removeClass("hidden")

        $(".ui_saveBtnBasicInfo").removeClass("hidden")
        $(".ui_cancelBtnBasicInfo").removeClass("hidden")
    })

    /*
        Basic Info --- Cancel Edit Button
    */
    $(".ui_cancelBtnBasicInfo").on("click", function(){
        $(".ui_basicInfoEmailText").removeClass("hidden")
        $(".ui_basicInfoUsernameText").removeClass("hidden")
        $(".ui_basicInfoFullNameText").removeClass("hidden")
        $(".ui_basicInfoRoleText").removeClass("hidden")
        
        $(this).addClass("hidden")
        $(".ui_saveBtnBasicInfo").addClass("hidden")

        $(".ui_basicInfoEmailInput").addClass("hidden")
        $(".ui_basicInfoUsernameInput").addClass("hidden")
        $(".ui_basicInfoFullNameInput").addClass("hidden")
        $(".ui_basicInfoRoleInput").addClass("hidden")

        
        $(".ui_editBtnBasicInfo").removeClass("hidden")
    })

    /*
        Basic Info --- Save Button
        TODO
    */


    /*
        Credential --- Edit Button
    */
    $(".ui_editBtnCredential").on("click", function(){
        $(".ui_credentialCurrentPasswordText").addClass("hidden")
        $(".ui_credentialNewPasswordText").addClass("hidden")
        $(".ui_credentialConfirmPasswordInput").addClass("hidden")

        $(this).addClass("hidden")

        $(".ui_credentialCurrentPasswordInput").removeClass("hidden")
        $(".ui_credentialNewPasswordInput").removeClass("hidden")
        $(".ui_credentialConfirmPasswordInput").removeClass("hidden")

        $(".ui_saveBtnCredential").removeClass("hidden")
        $(".ui_cancelBtnCredential").removeClass("hidden")
    })



    /*
        Credential --- Cancel Edit Button
    */
    $(".ui_cancelBtnCredential").on("click", function(){
        $(".ui_credentialCurrentPasswordText").removeClass("hidden")
        $(".ui_credentialNewPasswordText").removeClass("hidden")
        $(".ui_credentialConfirmPasswordInput").removeClass("hidden")

        $(this).addClass("hidden")
        $(".ui_saveBtnCredential").addClass("hidden")

        $(".ui_credentialCurrentPasswordInput").addClass("hidden")
        $(".ui_credentialNewPasswordInput").addClass("hidden")
        $(".ui_credentialConfirmPasswordInput").addClass("hidden")

        $(".ui_editBtnCredential").removeClass("hidden")
    })

    /*
        Credential --- Save Button
        TODO
    */


    /*
        Basic Stat --- Edit Button
    */
    $(".ui_editBtnBasicStat").on("click", function(){
        $(".ui_basicStatSendNotifStatusText").addClass("hidden")
        $(".ui_basicStatDingKeyText").addClass("hidden")
    
        $(this).addClass("hidden")

        $(".ui_basicStatSendNotifStatusInput").removeClass("hidden")
        $(".ui_basicStatDingKeyInput").removeClass("hidden")

        $(".ui_saveBtnBasicStat").removeClass("hidden")
        $(".ui_cancelBtnBasicStat").removeClass("hidden")
    })


    
})