 /*
    Menu Navigations
*/

    let menus = ['ui_contentUserManagement', 'ui_contentNotificationManagement', 'ui_contentSensorManagement']
    let menuNavs = ['ui_showUserManagement', 'ui_showNotificationManagement', 'ui_showSensorManagement']

    let currentMenu = menus[0]
    let currentMenuNavs = menuNavs[0]

    $(document).ready(function(){
        $('.ui_showUserManagement').on('click', function(){
            showContentAtIndex(0)
        })

        $('.ui_showNotificationManagement').on('click', function(){
            showContentAtIndex(1)
        })

        $('.ui_showSensorManagement').on('click', function(){
            showContentAtIndex(2)
        })
    })

    function showContentAtIndex(index){
        if(currentMenu != menus[index]){
            showContentExcept([index])
            currentMenu = menus[index]
        }

        for(let i = 0; i < menuNavs.length; i++){
            if (i == index){
                $('.'+menuNavs[i]).addClass('font-bold')
                continue
            }

            $('.'+menuNavs[i]).removeClass('font-bold')
        }
    }

    function showContentExcept(index){
        for (let i = 0; i < menus.length; i++){
            if(index.includes(i)){
                $('.'+menus[i]).removeClass("hidden")
                continue
            }

            $('.'+menus[i]).addClass("hidden")
        }
    }