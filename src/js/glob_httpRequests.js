const SERVER_HOST = "http://localhost:3001"

const LOGIN_URL = "/api/v1/auth/login"

function httpRequest_doLogin(username, password){
    return $.ajax({
        url: SERVER_HOST+LOGIN_URL,
        type: 'POST',
        data: JSON.stringify({'username': username, 'credential' : password}), 
        headers : {
            'Content-Type' : 'application/json; charset=utf-8'
        },
        dataType: 'json'
    })
}
